package com.example.demo.interfaces;

import com.example.demo.domain.Demande;
import com.example.demo.domain.DemandeRepository;
import com.example.demo.domain.DemandeV1;
import com.example.demo.domain.DemandeV2;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@Slf4j
@RequiredArgsConstructor
public class DemandeController {
    private final DemandeRepository demandeRepository;

    // Accept=*/* will map to v1 version
    @GetMapping(produces = "application/monapp.demande+json;version=1")
    Flux<Demande> allV1() {
        return demandeRepository.findAll(Example.of(DemandeV1.builder().build()));
    }

    @GetMapping(produces = "application/monapp.demande+json;version=2")
    Flux<Demande> allV2() {
        return demandeRepository.findAll(Example.of(DemandeV2.builder().build()));
    }

//    @GetMapping(produces = "application/monapp.demande+json;version=2")
//    Flux<Demande> allLast(@RequestHeader(value = "accept", required = false, defaultValue = "application/monapp.demande+json;version=2") String optionalHeader) {
//        return allV2()
//                .doOnNext(demande -> log.info("Demande : {}", demande));
//    }

}
