package com.example.demo;

import com.example.demo.domain.DemandeRepository;
import com.example.demo.domain.DemandeV1;
import com.example.demo.domain.DemandeV2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;

import java.time.Instant;

@SpringBootApplication
@Slf4j
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public SmartInitializingSingleton createData(DemandeRepository demandeRepository) {
        return () -> Mono.zip(demandeRepository.deleteAll(), demandeRepository.save(DemandeV1.builder().content("demandeV1-" + Instant.now().getEpochSecond()).build()),
                demandeRepository.save(DemandeV2.builder().currentInstant(Instant.now()).build())).subscribe();

    }

}
