package com.example.demo.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("demande")
@Data
public abstract class Demande {
    @Id
    private String id;
    @Version
    private Long version;
}
