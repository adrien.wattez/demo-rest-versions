package com.example.demo.domain;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface DemandeRepository extends ReactiveMongoRepository<Demande, String> {
}
