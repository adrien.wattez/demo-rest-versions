package com.example.demo.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.Instant;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Builder
// Globally NONE change forward + backward = NONE
public class DemandeV2 extends Demande {
    // Backward change -> Remove fields
    //private String content;
    // cf Schema evolution https://docs.confluent.io/platform/current/schema-registry/avro.html#summary
    // Forward change -> Add fields
    private Instant currentInstant;
}

